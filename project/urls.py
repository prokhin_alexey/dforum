from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'forum.views.show_root', name='show_root'),
    url(r'^category/(?P<category_id>\d+)/$', 'forum.views.show_category', name='show_category'),
    url(r'^category/(?P<category_id>\d+)/page(?P<page>\d+)/$', 'forum.views.show_category', name='show_category'),
    url(r'^topic/(?P<topic_id>\d+)/$', 'forum.views.show_topic', name='show_topic'),
    url(r'^add_topic/(?P<category_id>\d+)/$', 'forum.views.add_topic', name='add_topic'),
    url(r'^add_post/(?P<topic_id>\d+)/$', 'forum.views.add_post', name='add_post'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^login/$', 'forum.views.login', name='login'),
    url(r'^logout/$', 'forum.views.logout', name='logout'),
)
