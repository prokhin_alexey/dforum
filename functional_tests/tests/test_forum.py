from time import sleep
from selenium.webdriver.support.ui import WebDriverWait
from forum.forms import INVALID_USER_OR_PASSWORD_ERROR
from functional_tests.tests.base import FunctionalTest
from forum.tests.fixture import *
from django.core.urlresolvers import reverse
from django.contrib.auth import get_user_model

User = get_user_model()

class ForumTest(FunctionalTest):

    def setUp(self):
        create_categories()
        super().setUp()

    def test_categories(self):
        create_topics()

        # Edith just learned about a new forum for software developers
        # She goes to the site and sees that the board is organized by
        # categories and each category, in its turn, has subcategories.
        self.get_page('/')

        # She finds a "Python" subcategory (of course, in the "Web
        # Development") and opens it.
        categories = self.browser.find_elements_by_class_name('category')
        category = self.find_category('Web Development', categories)
        subcategories = category.find_elements_by_css_selector('.subcategory .title')

        python_subcategory = self.find_category('Python', subcategories)
        python_subcategory.find_element_by_link_text('Python').click()

        # A page with a list of topics is opened before her.
        python_category = TopicCategory.objects.get(title='Python')
        self.assert_current_url_equals(python_category.get_absolute_url())
        categories = self.browser.find_elements_by_class_name('category')
        category = self.find_category('Topics', categories)
        topics = category.find_elements_by_css_selector('.topic .title')

        # Edith finds an interesting topic and opens it
        django_topic_elem = self.find_category('Django', topics)
        django_topic_elem.find_element_by_link_text('Django').click()
        django_topic = Topic.objects.get(title='Django')
        self.assert_current_url_equals(django_topic.get_absolute_url())

    def test_topic(self):
        # The last time Edith was on the forum, she opened a topic to
        # ask a question.
        topic = create_topic_with_posts()

        # She goes straight to the topic page and finds
        # out that someone answered it.
        self.get_page(topic.get_absolute_url())
        posts = self.find_posts(2)
        self.check_post(posts[0], 'What is better?')
        self.check_post(posts[1], 'Edith, stop asking holy war questions!')

    def test_login_logout(self):
        topic = create_topic_with_posts()

        # Edith goes to the main page and enters her login and password.
        self.get_page('/')
        self.login()

        # She is redirected to the main page again, but this time,
        # she sees a message greeting her and logout link.
        self.assert_current_url_equals('/')
        user_box = self.browser.find_element_by_class_name('userinfo')
        self.assertIn('Edith', user_box.text)

        # Edith opens a post she previously opened, find out that
        # nobody posted new answers and logs out.
        self.get_page(topic.get_absolute_url())
        user_box = self.browser.find_element_by_class_name('userinfo')
        user_box.find_element_by_class_name('logout').click()

        # She is redirected to the topic page, the greeting message
        # is replaced by input boxes.
        self.assert_current_url_equals(topic.get_absolute_url())
        self.assertIsNotNone(self.browser.find_element_by_class_name('login'))

    def test_login_validation_and_redirect(self):
        topic = create_topic_with_posts()

        # Edith goes to the site to check her topic, then enters
        # her login and password.
        self.get_page(topic.get_absolute_url())
        self.login('edith', 'wrongPassword')

        # But this time she is redirected to a page, asking her to enter
        # her credentials again. There is also a message saying she
        # made a mistake in her username or password.
        self.assert_current_url_equals(reverse('login') + '?next=' + topic.get_absolute_url())
        login_forms = self.browser.find_elements_by_class_name('login')
        self.assertEqual(len(login_forms), 1)  # make sure that there is only one login form
        self.assertIn(INVALID_USER_OR_PASSWORD_ERROR, login_forms[0].text)

        # She reenters her login and password and this time the login is
        # successful, she is redirected to the topic page.
        self.login()
        self.assert_current_url_equals(topic.get_absolute_url())

    def test_last_message(self):
        topic = create_topic_with_posts()

        self.get_page('/')
        subcategories = self.browser.find_elements_by_css_selector('.subcategory')
        python_subcategory_elem = self.find_category('Python', subcategories)
        last_post = python_subcategory_elem.find_element_by_css_selector('.last_post')

        self.assertIn('max', last_post.text)
        last_post.find_element_by_link_text('Django vs Flask').click()
        self.assert_current_url_equals(topic.get_absolute_url())

    def test_pagination(self):
        category = create_category_with_a_lot_of_topics(90)

        def check_page(current_page, first_link, last_link, previous_is_active=True, next_is_active=True):
            self.assert_current_url_equals(category.get_absolute_url(current_page))

            paginations = self.browser.find_elements_by_class_name('pagination')
            for pagination in paginations:
                # check that there is links for pages between first_link and last_link
                for page in range(first_link, last_link+1):
                    if page == current_page:
                        current_page_elem = pagination.find_element_by_css_selector('.current_page')
                        self.assertEqual(current_page_elem.text, str(page))
                    else:
                        self.assertIsNotNone(pagination.find_element_by_link_text(str(page)))

                # ... but no more than that!
                self.assertEqual(len(pagination.find_elements_by_link_text(str(first_link-1))), 0)
                self.assertEqual(len(pagination.find_elements_by_link_text(str(last_link +1))), 0)

                # check 'previous' link
                previous = pagination.find_elements_by_css_selector('a.previous')
                self.assertNotEqual(len(previous) == 0, previous_is_active)

                # check 'next' link
                next = pagination.find_elements_by_css_selector('a.next')
                self.assertNotEqual(len(next) == 0, next_is_active)

            # check topics
            topics = self.browser.find_elements_by_css_selector('.topic .title')
            last_topic_id = 20 * current_page
            first_topic_id = last_topic_id - 20 + 1
            topic_ids = range(first_topic_id, min(last_topic_id, 90)+1)
            self.assertEqual(len(topic_ids), len(topics))
            for id, topic in zip(topic_ids, topics):
                self.assertIn(str(id), topic.text)

            return paginations[0]

        # Check first page
        self.get_page(category.get_absolute_url(1))
        p = check_page(1, 1, 3, False, True)

        # Check next button
        p.find_element_by_css_selector('.next').click()
        p = check_page(2, 1, 4)

        # Check page link
        p.find_element_by_link_text('3').click()
        p = check_page(3, 1, 5)

        # Check that the links to non-existent pages are not shown
        p.find_element_by_link_text('4').click()
        p = check_page(4, 2, 5)

        # Check the last page
        p.find_element_by_css_selector('.next').click()
        p = check_page(5, 3, 5, True, False)

        # Check 'previous' link is working
        p.find_element_by_css_selector('.previous').click()
        p = check_page(4, 2, 5)

    def test_create_post(self):
        topic = create_topic_with_posts()

        # Edith goes to the forum, logs in and sees that
        # there is an answer in her topic.
        self.get_page('/')
        self.login()
        self.get_page(topic.get_absolute_url())

        # She decides to answer and clicks on "Add reply" link
        self.browser.find_element_by_link_text("Add reply").click()

        # The page is reloaded and there is a text input.
        form = self.browser.find_element_by_class_name('post_form')
        post_input = form.find_element_by_name("message")

        # She types her answer in here and clicks "Reply"
        post_input.send_keys("Come on, Max, I'm just asking.")
        self.browser.find_element_by_css_selector("input[type='submit']").click()

        # Her topic appears again, at the bottom she sees her
        # new answer.
        self.assert_current_url_equals(topic.get_absolute_url())
        posts = self.find_posts(3)
        self.check_post(posts[2], "Come on, Max, I'm just asking.")

    def test_create_topic(self):
        get_edith()

        # Edith goes to the forum to ask a question.
        self.get_page('/')
        self.login()

        # She logs in, opens Python subforum and clicks
        # 'New topic' button.
        python = TopicCategory.objects.get(title='Python')
        self.get_page(python.get_absolute_url())
        self.browser.find_element_by_link_text("New topic").click()

        # She is redirected to a page with input boxes to enter
        # the new topic title and her first message.
        form = self.browser.find_element_by_class_name('topic_form')
        title_input = form.find_element_by_name('title')
        title_input.send_keys("Pyramid")
        post_input = form.find_element_by_name('message')
        post_input.send_keys("How good is that framework?")

        # As soon as she does that, she clicks 'Create topic'.
        self.browser.find_element_by_css_selector("input[type='submit']").click()

        # The new topic with her first message is appeared before her.
        self.browser.implicitly_wait(2)
        pyramid = Topic.objects.get(title='Pyramid')
        self.assert_current_url_equals(pyramid.get_absolute_url())
        posts = self.find_posts(1)
        self.check_post(posts[0], "How good is that framework?")

    def check_post(self, post, message):
        post_message = post.find_element_by_class_name('message')
        self.assertEqual(post_message.text, message)

    def find_category(self, needle, haystack):
        subcategory = None
        for i in haystack:
            if needle in i.text:
                subcategory = i
        self.assertIsNotNone(subcategory, 'Category %s is not found' % needle)
        return subcategory

    def find_posts(self, count):
        posts = self.browser.find_elements_by_class_name('post')
        self.assertEqual(len(posts), count)
        return posts

    def assert_current_url_equals(self, relative_url):
        self.assertEqual(self.browser.current_url, self.server_url + relative_url)

    def get_classes(self, elem):
        return elem.get_attribute('class').split(' ')

    def get_page(self, relative_url):
        self.browser.get(self.server_url + relative_url)

    def login(self, username='edith', password='inevertellmypasswords'):
        login_box = self.browser.find_element_by_class_name('login')
        username_elem = login_box.find_element_by_name('username')
        username_elem.clear()
        username_elem.send_keys(username)
        password_elem = login_box.find_element_by_name('password')
        password_elem.clear()
        password_elem.send_keys(password + '\n')
