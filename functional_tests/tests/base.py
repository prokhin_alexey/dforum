from django.test import LiveServerTestCase
from django.test.utils import override_settings
from selenium import webdriver
from project import settings
import sys


def remove_from_tuple(t, value):
    l = list(t)
    l.remove(value)
    return tuple(l)

INSTALLED_APPS = remove_from_tuple(
    settings.INSTALLED_APPS,
    'debug_toolbar'
)
MIDDLEWARE_CLASSES = remove_from_tuple(
    settings.MIDDLEWARE_CLASSES,
    'debug_toolbar.middleware.DebugToolbarMiddleware'
)

@override_settings(DEBUG=True, TEMPLATE_DEBUG=True,
                   INSTALLED_APPS=INSTALLED_APPS,
                   MIDDLEWARE_CLASSES=MIDDLEWARE_CLASSES)
class FunctionalTest(LiveServerTestCase):

    @classmethod
    def setUpClass(cls):
        for arg in sys.argv:
            if 'liveserver' in arg:
                cls.server_url = 'http://' + arg.split('=')[1]
                return
        LiveServerTestCase.setUpClass()
        cls.server_url = cls.live_server_url

    @classmethod
    def tearDownClass(cls):
        if cls.server_url == cls.live_server_url:
            LiveServerTestCase.tearDownClass()

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(1)

    def tearDown(self):
        self.browser.quit()

