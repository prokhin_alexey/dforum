from django.db import models
from django.shortcuts import resolve_url
from django.contrib.auth import models as auth_models
from mptt.models import MPTTModel, TreeForeignKey
from .nulls_last_manager import NullsLastManager

class TimeStampedModel(models.Model):
    creation_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now_add=True, auto_now=True)

    def save(self, *args, **kwargs):
        self.full_clean()
        super().save(*args, **kwargs)

    class Meta:
        abstract = True


class TopicNode(TimeStampedModel):
    """
    Category of topics or topic
    """
    title = models.CharField(max_length=255)
    subtitle = models.CharField(max_length=255, blank=True)

    def __str__(self):
        return self.title

    class Meta:
        abstract = True


class TopicCategory(MPTTModel, TopicNode):
    """
    Category of topics
    """
    parent = TreeForeignKey("self", default=None, blank=True, null=True)

    def get_absolute_url(self, page=None):
        if page:
            return resolve_url('show_category', category_id=self.id, page=page)
        else:
            return resolve_url('show_category', category_id=self.id)

    class MPTTMeta:
        order_insertion_by = ['title']


class Topic(TopicNode):
    """
    Topic
    """
    author = models.ForeignKey("User", blank=False, null=True)
    category = models.ForeignKey("TopicCategory", blank=False, null=True)
    views = models.PositiveIntegerField(default=0, blank=True)

    def get_absolute_url(self):
        return resolve_url('show_topic', topic_id=self.id)

    class Meta:
        ordering = ['title']

class Post(TimeStampedModel):
    author = models.ForeignKey("User", blank=False, null=True)
    topic = models.ForeignKey("Topic", blank=False, null=True)
    message = models.TextField(blank=False)

    class Meta:
        ordering = ['creation_time']

class User(auth_models.AbstractUser):
    def get_full_name(self):
        name = super().get_full_name()
        if not name:
            name = self.username
        return name

    class Meta(auth_models.AbstractUser.Meta):
        pass

class TopicWithLastPostView(models.Model):
    objects = NullsLastManager()

    title = models.CharField(max_length=255)
    post_count = models.IntegerField()
    category = models.ForeignKey("TopicCategory")
    last_post = models.ForeignKey("Post")
    last_post_creation_time = models.DateTimeField()
    last_post_author = models.ForeignKey("User")
    last_post_author_username = models.CharField(max_length=30)

    class Meta:
        db_table = "forum_topic_with_last_post_view"
        managed = False
        ordering = ['-last_post_creation_time', 'title']