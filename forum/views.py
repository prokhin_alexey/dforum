from django.contrib import auth
from django.core.paginator import Paginator, EmptyPage
from django.db import connection
from django.http import Http404
from django.shortcuts import render, redirect, get_object_or_404
from django.template import RequestContext
from forum.forms import LoginForm, AddPostForm, AddTopicForm
from forum.models import Topic, TopicWithLastPostView, Post, TopicCategory


def tuple_to_category(t):
    category_keys = (
        'id',
        'title',
        'subtitle',
        'topic_count',
        'post_count',
    )
    topic_keys = (
        'id',
        'date',
        'topic_id',
        'topic_title',
        'author_id',
        'author_username',
    )

    category = dict(zip(category_keys, t[0:5]))
    if t[5]:
        category['last_post'] = dict(zip(topic_keys, t[5:]))
    return category


def login_form_context_processor(request):
    val = dict()
    if request.user.is_anonymous():
        val['login_form'] = LoginForm()
    return val


def show_root(request):
    sql = """
        SELECT
            pc.id,
            pc.title,
            pc.subtitle,
            c.id,
            c.title,
            c.subtitle,
            c.topic_count,
            c.post_count,
            c.last_post_id,
            c.last_post_creation_time,
            c.last_post_topic_id,
            c.last_post_topic_title,
            c.last_post_author_id,
            c.last_post_author_username
        FROM forum_topiccategory as pc
        LEFT JOIN forum_category_view as c
        ON c.parent_id = pc.id
        WHERE pc.parent_id is NULL
        ORDER BY
            pc.tree_id ASC, pc.lft ASC,
            c.tree_id ASC, c.lft ASC
    """

    cursor = connection.cursor()
    cursor.execute(sql)

    topic_categories = list()
    for row in cursor.fetchall():
        category_id = row[0]
        if len(topic_categories) == 0 or topic_categories[-1]['id'] != category_id:
            keys = ('id', 'title', 'subtitle')
            category = dict(zip(keys, row[0:3]))
            category['subcategories'] = list()
            topic_categories.append(category)
        if id:
            category = tuple_to_category(row[3:])
            if category['id']:
                topic_categories[-1]['subcategories'].append(category)

    return render(request, 'show_root.html', {
        'topic_categories': topic_categories,
    })


def show_category(request, category_id, page=1):
    sql = """
        SELECT
            c.id,
            c.title,
            c.subtitle,
            c.topic_count,
            c.post_count,
            c.last_post_id,
            c.last_post_creation_time,
            c.last_post_topic_id,
            c.last_post_topic_title,
            c.last_post_author_id,
            c.last_post_author_username
        FROM forum_category_view as c
        WHERE c.parent_id = %s
        ORDER BY c.tree_id ASC, c.lft ASC
    """

    category = get_object_or_404(TopicCategory, id=category_id)

    cursor = connection.cursor()
    cursor.execute(sql, [category_id])
    topic_categories = [tuple_to_category(row) for row in cursor.fetchall()]

    topic_pages = Paginator(TopicWithLastPostView.objects.filter(category_id=category_id), 20)
    try:
        topics = topic_pages.page(page)
    except EmptyPage:
        raise Http404

    return render(request, 'show_category.html', {
        'category': category,
        'topic_categories': topic_categories,
        'topics': topics,
    })


def show_topic(request, topic_id):
    topic = get_object_or_404(Topic, id=topic_id)
    topic.views += 1
    topic.save()

    return render(request, 'show_topic.html', {
        'topic': topic,
        'posts': Post.objects.filter(topic=topic_id).select_related('author')
    })


def add_post(request, topic_id):
    topic = get_object_or_404(Topic, id=topic_id)
    if request.method == 'POST':
        form = AddPostForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            Post.objects.create(message=data['message'], author=request.user, topic=topic)
            return redirect(topic.get_absolute_url())
    else:
        form = AddPostForm()

    return render(request, 'add_post.html', {
        'topic': topic,
        'form': form,
    })


def add_topic(request, category_id):
    category = get_object_or_404(TopicCategory, id=category_id)
    if request.method == 'POST':
        form = AddTopicForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            topic = Topic.objects.create(title=data['title'], author=request.user, category=category)
            Post.objects.create(message=data['message'], author=request.user, topic=topic)
            return redirect(topic.get_absolute_url())
    else:
        form = AddTopicForm()

    return render(request, 'add_topic.html', {
        'category': category,
        'form': form,
    })

def login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            auth.login(request, form.user)
            return redirect(request.GET.get('next', 'show_root'))
    else:
        form = LoginForm()

    return render(request, 'login.html', RequestContext(request, {
        'form': form,
    }))


def logout(request):
    auth.logout(request)
    return redirect(request.GET.get('next', 'show_root'))

