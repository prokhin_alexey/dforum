from django.shortcuts import resolve_url
from django.test import TestCase
from forum.models import TopicCategory
from forum.tests.fixture import *


class BaseTestCase(object):
    def login_as(self, user):
        user.set_password('password')
        user.save()
        self.client.login(username=user.username, password='password')

class ShowCategoryTest(TestCase):

    def setUp(self):
        create_categories()
        create_topics()
        super().setUp()

    def test_root_categories(self):
        create_topic_with_posts()
        last_post = create_topic_in_subsubcategory()

        response = self.client.get(resolve_url('show_root'))
        categories = response.context['topic_categories']

        self.assertEqual(len(categories), 2)
        self.assertEqual(categories[0]['title'], "Software Development")
        self.assertEqual(categories[1]['title'], "Web Development")

        subcategories = categories[0]['subcategories']
        self.assertEqual(len(subcategories), 2)
        self.check_category(subcategories[0], "C#")
        self.check_category(subcategories[1], "C++")

        subcategories = categories[1]['subcategories']
        self.assertEqual(len(subcategories), 2)
        self.check_category(subcategories[0], "PHP")
        self.check_category(subcategories[1], "Python", 4, 3, self.post_to_dict(last_post))

    def test_subcategory(self):
        create_topic_with_posts()
        last_post = create_topic_in_subsubcategory()

        category = TopicCategory.objects.get(title='Web Development')
        response = self.client.get(category.get_absolute_url())
        categories = response.context['topic_categories']

        self.assertEqual(len(categories), 2)
        self.check_category(categories[0], "PHP")
        self.check_category(categories[1], "Python", 4, 3, self.post_to_dict(last_post))

    def test_topics(self):
        category = TopicCategory.objects.get(title='Python')
        response = self.client.get(category.get_absolute_url())
        topics = response.context['topics']

        self.assertEqual(len(topics), 2)
        self.assertEqual(topics[0].title, "Django")
        self.assertEqual(topics[1].title, "Flask")

    def test_last_post(self):
        topic = create_topic_with_posts()
        post = topic.post_set.get(author=get_max())

        category = TopicCategory.objects.get(title='Web Development')
        response = self.client.get(category.get_absolute_url())
        categories = response.context['topic_categories']

        self.check_category(categories[1], "Python", 3, 2, self.post_to_dict(post))

    def check_category(self, category, title, topics=0, posts=0, last_post=None):
        self.assertEqual(category['title'], title)
        self.assertEqual(category['topic_count'], topics)
        self.assertEqual(category['post_count'], posts)
        if last_post:
            self.assertDictEqual(category['last_post'], last_post)

    def post_to_dict(self, post):
        return {
            'id': post.id,
            'date': post.creation_time,
            'topic_id': post.topic_id,
            'topic_title': post.topic.title,
            'author_id': post.author_id,
            'author_username': post.author.username,
        }

class ShowTopicTest(TestCase):
    def setUp(self):
        create_categories()
        create_topics()
        self.topic = create_topic_with_posts()
        super().setUp()

    def test_topics(self):
        response = self.client.get(self.topic.get_absolute_url())
        posts = response.context['posts']

        self.assertEqual(len(posts), 2)
        self.assertEqual(posts[0].message, 'What is better?')
        self.assertEqual(posts[1].message, 'Edith, stop asking holy war questions!')

class AddTopicTest(TestCase, BaseTestCase):
    def setUp(self):
        create_categories()
        self.category = TopicCategory.objects.get(title='Python')

    def test_topic_is_created(self):
        user = get_edith()
        self.login_as(user)

        self.add_topic({
            'title': 'title',
            'message': 'message',
        })

        new_topic = Topic.objects.get(title='title', category=self.category, author=user)
        self.assertIsNotNone(new_topic)
        self.assertIsNotNone(Post.objects.get(message='message', topic=new_topic, author=user))

    def test_redirect(self):
        self.login_as(get_edith())

        response = self.add_topic({
            'title': 'title',
            'message': 'message',
        })

        topic = Topic.objects.all()[0]
        self.assertRedirects(response, topic.get_absolute_url())

    def add_topic(self, data):
        return self.client.post(resolve_url('add_topic', category_id=self.category.id), data=data)


class AddPostTest(TestCase, BaseTestCase):
    def setUp(self):
        create_categories()
        self.topic = create_topic_with_posts()

    def test_post_is_created(self):
        user = get_edith()
        self.login_as(user)

        self.add_post({ 'message': 'message' })

        self.assertIsNotNone(Post.objects.get(message='message', topic=self.topic, author=user))

    def test_redirect(self):
        self.login_as(get_edith())

        response = self.add_post({ 'message': 'message' })

        self.assertRedirects(response, self.topic.get_absolute_url())

    def add_post(self, data):
        return self.client.post(resolve_url('add_post', topic_id=self.topic.id), data=data)