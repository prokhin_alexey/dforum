from django.test import TestCase
from forum.forms import *
from django.contrib.auth import get_user_model

User = get_user_model()


class TestLoginForm(TestCase):
    def test_user_validation(self):
        User.objects.create_user('test', password='myPassword')
        form = LoginForm({'username': 'test', 'password': 'myPassword'})
        self.assertTrue(form.is_valid())

    def test_invalid_username_failure(self):
        form = LoginForm({'username': 'test', 'password': 'myPassword'})
        self.assert_validation_error(form, INVALID_USER_OR_PASSWORD_ERROR)

    def test_invalid_password_failure(self):
        User.objects.create_user('test', password='myPassword')
        form = LoginForm({'username': 'test', 'password': 'wrongPassword'})
        self.assert_validation_error(form, INVALID_USER_OR_PASSWORD_ERROR)

    def test_disabled_account_failure(self):
        user = User.objects.create_user('test', password='myPassword')
        user.is_active = False
        user.save()
        form = LoginForm({'username': 'test', 'password': 'myPassword'})

        self.assert_validation_error(form, DISABLED_ACCOUNT_ERROR)

    def assert_validation_error(self, form, message):
        self.assertFalse(form.is_valid())
        errors = form.non_field_errors()
        self.assertEqual(len(errors), 1)
        self.assertEqual(errors[0], message)