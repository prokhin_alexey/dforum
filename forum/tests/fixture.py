from forum.models import TopicCategory, Topic, Post, User


def get_user(username, **kwargs):
    user = User.objects.filter(username=username)
    if user:
        return user[0]
    return User.objects.create_user(username, **kwargs)


def get_edith():
    return get_user(
        'edith',
        email='edith@example.com',
        password='inevertellmypasswords',
        first_name='Edith',
    )


def get_max():
    return get_user(
        'max',
        email='max@example.com',
        password='secret',
        first_name='Max',
    )


def create_categories():
    web_dev = TopicCategory.objects.create(title='Web Development')
    TopicCategory.objects.create(title='PHP', parent=web_dev)
    python = TopicCategory.objects.create(title='Python', parent=web_dev)
    TopicCategory.objects.create(title='Work', parent=python)

    soft_dev = TopicCategory.objects.create(title='Software Development')
    TopicCategory.objects.create(title='C++', parent=soft_dev)
    TopicCategory.objects.create(title='C#', parent=soft_dev)


def create_topics():
    user = get_edith()
    python = TopicCategory.objects.get(title='Python')
    Topic.objects.create(title='Django', category=python, author=user)
    Topic.objects.create(title='Flask', category=python, author=user)


def create_topic_with_posts():
    edith = get_edith()
    max = get_max()
    python = TopicCategory.objects.get(title='Python')
    topic = Topic.objects.create(title='Django vs Flask', category=python, author=edith)
    Post.objects.create(message='What is better?', topic=topic, author=edith)
    Post.objects.create(message='Edith, stop asking holy war questions!', topic=topic, author=max)
    return topic


def create_topic_in_subsubcategory():
    user = get_max()
    work = TopicCategory.objects.get(title='Work')
    topic = Topic.objects.create(title='In search for developer', category=work, author=user)
    return Post.objects.create(message='...', topic=topic, author=user)


def create_category_with_a_lot_of_topics(number):
    user = get_max()
    category = TopicCategory.objects.create(title='A lot of topics')
    for i in range(1, number+1):
        Topic.objects.create(title='Topic #{:02d}'.format(i), category=category, author=user)
    return category