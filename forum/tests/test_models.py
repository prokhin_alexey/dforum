from django.utils import timezone
from django.test import TestCase
from django.core.exceptions import ValidationError
from forum.models import TopicCategory, Topic, Post, TopicWithLastPostView
from django.contrib.auth import get_user_model
from .fixture import create_topics, create_categories, create_topic_with_posts, get_max

User = get_user_model()


class ModelTest(object):
    def assert_require_field(self, field):
        model = self.model_type()
        with self.assertRaises(ValidationError) as e:
            model.save()
        self.assertIn(field, e.exception.message_dict)


class TimeStampedModelTest(ModelTest):
    factory = None

    def test_creation_time_is_set(self):
        group = self.factory()
        group.save()
        self._check_time_stamp(group.creation_time)

    def test_update_time_is_set(self):
        group = self.factory()
        group.save()
        self._check_time_stamp(group.update_time)

    def test_update_time_is_updated(self):
        group = self.factory()
        group.save()
        group.title = "new name"
        group.save()
        self._check_time_stamp(group.update_time)

    def _check_time_stamp(self, time):
        self.assertLess(abs((time - timezone.now()).total_seconds()), 1)


class TopicNodeTest(TimeStampedModelTest):
    def test_saving_and_retrieving(self):
        first = self.factory()
        first.title = "First"
        first.save()

        second = self.factory()
        second.title = "Second"
        second.save()

        saved = self.model_type.objects.all()
        self.assertEqual(saved.count(), 2)
        self.assertEqual(saved[0].title, "First")
        self.assertEqual(saved[1].title, "Second")

    def test_to_unicode(self):
        node = self.factory()
        self.assertEqual(node.title, str(node))

    def test_topic_group_must_have_title(self):
        self.assert_require_field('title')


class TopicCategoryTest(TestCase, TopicNodeTest):
    model_type = TopicCategory

    def factory(self):
        return TopicCategory.objects.create(title="Group")

    def test_objects_all_returns_only_groups(self):
        self.factory()

        groups = TopicCategory.objects.all()

        self.assertEqual(groups.count(), 1)
        self.assertEqual(groups[0].title, "Group")

    def test_get_absolute_url_for_first_page(self):
        group = TopicCategory.objects.create(title='title')
        self.assertEqual(group.get_absolute_url(), '/category/%d/' % (group.id,))

    def test_get_absolute_url_for_second_page(self):
        group = TopicCategory.objects.create(title='title')
        self.assertEqual(group.get_absolute_url(2), '/category/%d/page2/' % (group.id))


class TopicTest(TestCase, TopicNodeTest):
    model_type = Topic

    def setUp(self):
        self.user = User.objects.create_user(username='Name')
        self.category = TopicCategory.objects.create(title="Group")
        super(TestCase, self).setUp()

    def factory(self, title="Topic"):
        return Topic(title=title, category=self.category, author=self.user)

    def test_objects_all_returns_only_topics(self):
        topic = self.factory()
        topic.save()

        topics = Topic.objects.all()

        self.assertEqual(topics.count(), 1)
        self.assertEqual(topics[0].title, "Topic")

    def test_get_absolute_url(self):
        topic = self.factory()
        topic.save()
        self.assertEqual(topic.get_absolute_url(), '/topic/%d/' % (topic.id,))

    def test_topic_must_have_title(self):
        self.assert_require_field('title')

    def test_topic_must_have_category(self):
        self.assert_require_field('category')

    def test_topic_must_have_author(self):
        self.assert_require_field('author')

    def test_views_zero_by_default(self):
        topic = self.factory()
        topic.save()
        self.assertEqual(topic.views, 0)


class TopicWithLastPostViewTest(TestCase):
    def test_objects_all(self):
        create_categories()
        create_topics()
        topic = create_topic_with_posts()
        user = get_max()
        post = topic.post_set.get(author=user)


        def check_topic(topicExt, title, post_count):
            self.assertEqual(topicExt.title, title)
            self.assertEqual(topicExt.post_count, post_count)
            self.assertEqual(topicExt.category_id, topic.category_id)
            if post_count > 0:
                self.assertEqual(topicExt.last_post_id, post.id)
                self.assertEqual(topicExt.last_post_creation_time, post.creation_time)
                self.assertEqual(topicExt.last_post_author_id, user.id)
                self.assertEqual(topicExt.last_post_author_username, user.username)
            else:
                self.assertEqual(topicExt.last_post_id, None)

        topics = TopicWithLastPostView.objects.all()
        self.assertEqual(len(topics), 3)
        check_topic(topics[0], 'Django vs Flask', 2)
        check_topic(topics[1], 'Django', 0)
        check_topic(topics[2], 'Flask', 0)


class PostTest(TestCase, TimeStampedModelTest):
    model_type = Post

    def setUp(self):
        category = TopicCategory.objects.create(title="Group")
        self.user = User.objects.create_user(username='Name')
        self.topic = Topic.objects.create(title="Topic", category=category, author=self.user)
        super(TestCase, self).setUp()

    def factory(self, message='Message'):
        return Post(message=message, topic=self.topic, author=self.user)

    def test_saving_and_retrieving(self):
        self.factory(message='msg1').save()
        self.factory(message='msg2').save()

        posts = Post.objects.all()

        self.assertEqual(posts.count(), 2)
        self.assertEqual(posts[0].message, "msg1")
        self.assertEqual(posts[1].message, "msg2")

    def test_post_must_have_topic(self):
        self.assert_require_field('topic')

    def test_post_must_have_author(self):
        self.assert_require_field('author')


class UserTest(TestCase):

    def test_get_full_name_returns_username_if_first_or_last_name_are_not_set(self):
        user = User.objects.create_user(username='Name')
        self.assertEqual(user.get_full_name(), 'Name')
