from django.utils.translation import ugettext as _
from django import forms
from django.core.exceptions import ValidationError
from django.contrib import auth

User = auth.get_user_model()

INVALID_USER_OR_PASSWORD_ERROR = _('Invalid username or password')
DISABLED_ACCOUNT_ERROR = _('Disabled account')


class LoginForm(forms.Form):
    username = forms.CharField(max_length=30, min_length=4)
    password = forms.CharField(min_length=4, widget=forms.PasswordInput())
    user = None

    def clean(self):
        cleaned_data = super(LoginForm, self).clean()
        username = cleaned_data.get("username")
        password = cleaned_data.get("password")

        if username and password:
            self.user = auth.authenticate(username=username, password=password)
            if self.user is not None:
                if not self.user.is_active:
                    raise ValidationError(DISABLED_ACCOUNT_ERROR)
            else:
                raise ValidationError(INVALID_USER_OR_PASSWORD_ERROR)

        return cleaned_data


class AddPostForm(forms.Form):
    message = forms.CharField(widget=forms.Textarea)


class AddTopicForm(forms.Form):
    title = forms.CharField(max_length=255)
    message = forms.CharField(widget=forms.Textarea)
