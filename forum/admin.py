from forum.models import TopicCategory, Topic
from django.contrib import admin
from django_mptt_admin import admin as mpttadmin


class TopicGroupAdmin(mpttadmin.DjangoMpttAdmin):
    tree_title_field = 'title'
    tree_display = ('title', 'subtitle')
    list_display = ('title', 'subtitle')

    class Meta:
        model = TopicCategory

admin.site.register(TopicCategory, TopicGroupAdmin)


class TopicAdmin(admin.ModelAdmin):
    list_display = ('title', 'subtitle')

admin.site.register(Topic, TopicAdmin)

