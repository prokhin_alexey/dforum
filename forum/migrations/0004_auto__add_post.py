# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'TopicGroup'
        db.delete_table('forum_topicgroup')

        # Adding model 'Post'
        db.create_table('forum_post', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('creation_time', self.gf('django.db.models.fields.DateTimeField')(blank=True, auto_now_add=True)),
            ('update_time', self.gf('django.db.models.fields.DateTimeField')(blank=True, auto_now_add=True, auto_now=True)),
            ('topic', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['forum.Topic'], null=True)),
            ('message', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('forum', ['Post'])

        # Adding model 'TopicCategory'
        db.create_table('forum_topiccategory', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('creation_time', self.gf('django.db.models.fields.DateTimeField')(blank=True, auto_now_add=True)),
            ('update_time', self.gf('django.db.models.fields.DateTimeField')(blank=True, auto_now_add=True, auto_now=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('subtitle', self.gf('django.db.models.fields.CharField')(blank=True, max_length=255)),
            ('parent', self.gf('mptt.fields.TreeForeignKey')(blank=True, default=None, to=orm['forum.TopicCategory'], null=True)),
            ('lft', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('rght', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('tree_id', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('level', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
        ))
        db.send_create_signal('forum', ['TopicCategory'])

        # Deleting field 'Topic.tree_id'
        db.delete_column('forum_topic', 'tree_id')

        # Deleting field 'Topic.level'
        db.delete_column('forum_topic', 'level')

        # Deleting field 'Topic.lft'
        db.delete_column('forum_topic', 'lft')

        # Deleting field 'Topic.parent'
        db.delete_column('forum_topic', 'parent_id')

        # Deleting field 'Topic.rght'
        db.delete_column('forum_topic', 'rght')

        # Adding field 'Topic.category'
        db.add_column('forum_topic', 'category',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['forum.TopicCategory'], null=True),
                      keep_default=False)


    def backwards(self, orm):
        # Adding model 'TopicGroup'
        db.create_table('forum_topicgroup', (
            ('update_time', self.gf('django.db.models.fields.DateTimeField')(blank=True, auto_now=True, auto_now_add=True)),
            ('tree_id', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('level', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('lft', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('parent', self.gf('mptt.fields.TreeForeignKey')(blank=True, default=None, to=orm['forum.TopicGroup'], null=True)),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('subtitle', self.gf('django.db.models.fields.CharField')(blank=True, max_length=255)),
            ('rght', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('creation_time', self.gf('django.db.models.fields.DateTimeField')(blank=True, auto_now_add=True)),
        ))
        db.send_create_signal('forum', ['TopicGroup'])

        # Deleting model 'Post'
        db.delete_table('forum_post')

        # Deleting model 'TopicCategory'
        db.delete_table('forum_topiccategory')


        # User chose to not deal with backwards NULL issues for 'Topic.tree_id'
        raise RuntimeError("Cannot reverse this migration. 'Topic.tree_id' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Topic.tree_id'
        db.add_column('forum_topic', 'tree_id',
                      self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'Topic.level'
        raise RuntimeError("Cannot reverse this migration. 'Topic.level' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Topic.level'
        db.add_column('forum_topic', 'level',
                      self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'Topic.lft'
        raise RuntimeError("Cannot reverse this migration. 'Topic.lft' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Topic.lft'
        db.add_column('forum_topic', 'lft',
                      self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True),
                      keep_default=False)

        # Adding field 'Topic.parent'
        db.add_column('forum_topic', 'parent',
                      self.gf('mptt.fields.TreeForeignKey')(blank=True, default=None, to=orm['forum.TopicGroup'], null=True),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'Topic.rght'
        raise RuntimeError("Cannot reverse this migration. 'Topic.rght' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Topic.rght'
        db.add_column('forum_topic', 'rght',
                      self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True),
                      keep_default=False)

        # Deleting field 'Topic.category'
        db.delete_column('forum_topic', 'category_id')


    models = {
        'forum.post': {
            'Meta': {'object_name': 'Post'},
            'creation_time': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.TextField', [], {}),
            'topic': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['forum.Topic']", 'null': 'True'}),
            'update_time': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True', 'auto_now': 'True'})
        },
        'forum.topic': {
            'Meta': {'ordering': "['title']", 'object_name': 'Topic'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['forum.TopicCategory']", 'null': 'True'}),
            'creation_time': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'subtitle': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '255'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'update_time': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True', 'auto_now': 'True'})
        },
        'forum.topiccategory': {
            'Meta': {'object_name': 'TopicCategory'},
            'creation_time': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'default': 'None', 'to': "orm['forum.TopicCategory']", 'null': 'True'}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'subtitle': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '255'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'update_time': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True', 'auto_now': 'True'})
        }
    }

    complete_apps = ['forum']