# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        db.execute("""
            CREATE VIEW forum_topic_view AS
            SELECT
                t.id,
                t.title,
                c.id      AS category_id,
                c.tree_id AS category_tree_id,
                c.lft     AS category_lft
            FROM forum_topic AS t
            JOIN forum_topiccategory AS c
            ON t.category_id = c.id
        """)

        db.execute("""
            CREATE VIEW forum_topic_with_last_post_view AS
            SELECT
                t.id,
                t.title,
                t.category_id,
                (
                    SELECT COUNT(*)
                    FROM forum_post
                    WHERE topic_id = t.id
                ) AS post_count,
                p.id            AS last_post_id,
                p.creation_time AS last_post_creation_time,
                u.id            AS last_post_author_id,
                u.username      AS last_post_author_username
            FROM forum_topic AS t
            LEFT JOIN forum_post AS p
            ON p.id = (
                SELECT id
                FROM forum_post
                WHERE topic_id = t.id
                ORDER BY creation_time DESC
                LIMIT 1
            )
            LEFT JOIN forum_user AS u
            ON p.author_id = u.id
        """)

        db.execute("""
            CREATE VIEW forum_category_view AS

            WITH post AS (
                SELECT
                    p.id,
                    p.creation_time,
                    t.category_id,
                    t.category_tree_id,
                    t.category_lft,
                    t.id         AS topic_id,
                    t.title      AS topic_title,
                    u.id         AS author_id,
                    u.username   AS author_username
                FROM forum_post AS p
                JOIN forum_topic_view AS t
                ON p.topic_id = t.id
                JOIN forum_user AS u
                ON p.author_id = u.id
            )

            SELECT
                c.id,
                c.title,
                c.subtitle,
                c.parent_id,
                c.tree_id,
                c.lft,
                c.rght,
                (
                    SELECT COUNT(*)
                    FROM forum_topic_view
                    WHERE
                        category_tree_id = c.tree_id AND
                        category_lft >= c.lft AND
                        category_lft <= c.rght
                ) AS topic_count,
                (
                    SELECT COUNT(*)
                    FROM post
                    WHERE
                        category_tree_id = c.tree_id AND
                        category_lft >= c.lft AND
                        category_lft <= c.rght
                ) AS post_count,
                lp.id              AS last_post_id,
                lp.creation_time   AS last_post_creation_time,
                lp.topic_id        AS last_post_topic_id,
                lp.topic_title     AS last_post_topic_title,
                lp.author_id       AS last_post_author_id,
                lp.author_username AS last_post_author_username
            FROM forum_topiccategory as c
            LEFT JOIN post as lp
            ON lp.id = (
                SELECT id
                FROM post
                WHERE
                    category_tree_id = c.tree_id AND
                    category_lft >= c.lft AND
                    category_lft <= c.rght
                ORDER BY creation_time DESC
                LIMIT 1
            )
        """)


    def backwards(self, orm):
        db.execute("DROP VIEW forum_category_view")
        db.execute("DROP VIEW forum_topic_with_last_post_view")
        db.execute("DROP VIEW forum_topic_view")


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '80', 'unique': 'True'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'blank': 'True', 'to': "orm['auth.Permission']"})
        },
        'auth.permission': {
            'Meta': {'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission', 'ordering': "('content_type__app_label', 'content_type__model', 'codename')"},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'contenttypes.contenttype': {
            'Meta': {'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'ordering': "('name',)", 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'forum.post': {
            'Meta': {'object_name': 'Post'},
            'author': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['forum.User']", 'null': 'True'}),
            'creation_time': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.TextField', [], {}),
            'topic': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['forum.Topic']", 'null': 'True'}),
            'update_time': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True', 'auto_now': 'True'})
        },
        'forum.topic': {
            'Meta': {'object_name': 'Topic', 'ordering': "['title']"},
            'author': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['forum.User']", 'null': 'True'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['forum.TopicCategory']", 'null': 'True'}),
            'creation_time': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'subtitle': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'update_time': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True', 'auto_now': 'True'}),
            'views': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0', 'blank': 'True'})
        },
        'forum.topiccategory': {
            'Meta': {'object_name': 'TopicCategory'},
            'creation_time': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'default': 'None', 'blank': 'True', 'null': 'True', 'to': "orm['forum.TopicCategory']"}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'subtitle': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'update_time': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True', 'auto_now': 'True'})
        },
        'forum.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'blank': 'True', 'to': "orm['auth.Group']", 'related_name': "'user_set'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'blank': 'True', 'to': "orm['auth.Permission']", 'related_name': "'user_set'"}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '30', 'unique': 'True'})
        }
    }

    complete_apps = ['forum']