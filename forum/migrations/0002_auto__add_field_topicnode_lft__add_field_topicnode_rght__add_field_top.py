# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'TopicNode.lft'
        db.add_column('forum_topicnode', 'lft',
                      self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True, default=0),
                      keep_default=False)

        # Adding field 'TopicNode.rght'
        db.add_column('forum_topicnode', 'rght',
                      self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True, default=0),
                      keep_default=False)

        # Adding field 'TopicNode.tree_id'
        db.add_column('forum_topicnode', 'tree_id',
                      self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True, default=0),
                      keep_default=False)

        # Adding field 'TopicNode.level'
        db.add_column('forum_topicnode', 'level',
                      self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True, default=0),
                      keep_default=False)


        # Changing field 'TopicNode.parent'
        db.alter_column('forum_topicnode', 'parent_id', self.gf('mptt.fields.TreeForeignKey')(to=orm['forum.TopicNode'], null=True))

    def backwards(self, orm):
        # Deleting field 'TopicNode.lft'
        db.delete_column('forum_topicnode', 'lft')

        # Deleting field 'TopicNode.rght'
        db.delete_column('forum_topicnode', 'rght')

        # Deleting field 'TopicNode.tree_id'
        db.delete_column('forum_topicnode', 'tree_id')

        # Deleting field 'TopicNode.level'
        db.delete_column('forum_topicnode', 'level')


        # Changing field 'TopicNode.parent'
        db.alter_column('forum_topicnode', 'parent_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['forum.TopicNode'], null=True))

    models = {
        'forum.topicnode': {
            'Meta': {'object_name': 'TopicNode'},
            'creation_time': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'to': "orm['forum.TopicNode']", 'blank': 'True', 'related_name': "'children'", 'default': 'None', 'null': 'True'}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'subtitle': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '255'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'type': ('django.db.models.fields.IntegerField', [], {'max_length': '1'}),
            'update_time': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True', 'auto_now_add': 'True'})
        }
    }

    complete_apps = ['forum']